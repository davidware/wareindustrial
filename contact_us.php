<?php
require_once 'library/php/Mobile_Detect.php';
require_once 'library/php/action.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();

//phpinfo();

/*
// Start the session
session_start();

if (isset($_SESSION['fName'])){
	echo($_SESSION['fName']);
}
*/
 
/*value="<?php if(isset($_POST['firstname'])){echo $_POST['firstname'];} ?>"*/
//echo $_POST['message'];
?>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
<meta charset="utf-8" />
<meta name="keywords" content="Ware Industrial, manufacturers representative, manufacturer's representative, southeast,  southeast manufacturer's representative, power transmission sales, bearing sales, engineering class chain, linear bearings, variable frequency inverters" />
<meta name="description" element="The Ware Industrial team has a broad knowledge of the local market.  Factory training in multiple product lines makes us an excellent resource for our customers." />

<title>Ware Industrial | Southeastern Engineered Product Representatives</title>

<?php include 'library/php/scripts_top.php'; ?>

<link rel="shortcut icon" href="library/images/favicon.ico">
<link rel="canonical" href="http://www.wareindustrial.com/" />
<script src="library/js/contact-ck.js"></script>
<script>
	var RecaptchaOptions = {
	   theme : 'clean',
	   tabindex : 7
	};
</script>
</head>
<body id="contact_us">
<!-- TODO: Client Side Javascript Validation
			reCAPTCHA error message
			style submit button
			style error messages
			style reCAPTCHA
			
			code Thank You response:
			Thank you for your intrest in Ware Industrial Sales. Someone will respond to your inquery as soon as possible.
			 -->

<?php include 'library/php/masthead.php'; ?>
	<div id="content" class="content clearfix">
		<h1>Contact Ware Industrial</h1>
		<form id="contact_us_form" action="contact_us.php" method="post">
			<legend>To contact Ware Industrial please fill out the form below.</legend>
			<fieldset>
				<label for="first_name">First Name</label><input type="text" placeholder="Enter Your First Name" id="first_name" name="first_name" value="<?php if(isset($_POST['fname'])){echo $_POST['fname'];} ?>" autofocus required tabindex="1"><br />
				<label for="last_name">Last Name</label><input type="text" placeholder="Enter Your Last Name"  id="last_name" name="last_name" value="<?php if(isset($_POST['lname'])){echo $_POST['lname'];} ?>" tabindex="2" required><br />
				<label for="company">Company</label><input type="text" placeholder="Enter Your Company's Name"  id="company" name="company" value="<?php if(isset($_POST['company'])){echo $_POST['company'];} ?>" tabindex="3"><br />
				<label for="email">E-mail</label><input type="text" id="email"name="email">
				<label for="from_email">E-mail</label><input type="email" placeholder="Enter Your E-mail Address"  id="from_email"name="from_email" value="<?php if(isset($_POST['email1'])){echo $_POST['email1'];} ?>" tabindex="4" required><br />
				<label for="phone">Phone</label><input type="tel"  placeholder="Enter Your Phone Number" id="phone" name="phone" value="<?php if(isset($_POST['phone'])){echo $_POST['phone'];} ?>" tabindex="5"><br />
				<label for="message">Message</label><textarea rows="4" cols="3" id="message" name="message" tabindex="6"><?php if(isset($_POST['message'])){echo $_POST['message'];} ?></textarea><br />
				<?php
		          require_once('library/php/recaptchalib.php');
		          $publickey = "6LeOy_sSAAAAAE_fvax3QX4JmUyz-VcQRn_gaLIU"; // you got this from the signup page
		          echo recaptcha_get_html($publickey);
		        ?>
		        <input id="submit" class="btn-submit" name="submit" value="Send" type="submit">
			</fieldset>
		</form>
	</div>

<?php include 'library/php/footer.php'; ?>
<?php include 'library/php/scripts_btm.php'; ?>
</body>
</html>