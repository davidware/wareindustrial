<?php
require_once 'library/php/Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();
?>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
<meta charset="utf-8" />
<meta name="keywords" content="Ware Industrial, manufacturers representative, manufacturer's representative, southeast,  southeast manufacturer's representative, power transmission sales, bearing sales, engineering class chain, linear bearings, variable frequency inverters" />
<meta name="description" element="The Ware Industrial team has a broad knowledge of the local market.  Factory training in multiple product lines makes us an excellent resource for our customers." />

<title>Ware Industrial | Southeastern Engineered Product Representatives</title>

<?php include 'library/php/scripts_top.php'; ?>

<link rel="shortcut icon" href="library/images/favicon.ico">
<link rel="canonical" href="http://www.wareindustrial.com/" />

</head>

<body id="products">
<?php include 'library/php/masthead.php'; ?>
  <div id="lineCard" class="lineCard clearfix">

  </div>

<?php include 'library/php/footer.php'; ?>

<?php include 'library/php/scripts_btm.php'; ?>
<script src="library/js/masonry.pkgd-ck.js"></script>
<script src="library/js/products-ck.js"></script>
  
</body>
</html>