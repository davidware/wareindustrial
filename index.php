<?php
require_once 'library/php/Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();
?>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
<meta charset="utf-8" />
<meta name="google-site-verification" content="EYMNA7ejU82WuuAhVKG5KPah-KHvCOvfdnokbrsYdnM" />
<meta name="keywords" content="Ware Industrial, manufacturers representative, manufacturer&#8217;s representative, southeast, power transmission sales, bearing sales, engineering class chain, linear bearings, variable frequency inverters" />
<meta name="description" content="Ware Industrial Sales is a manufacturer&#8217;s representative of engineered and industrial products including power transmission, bearings and linear motion. Serving the Southeastern United States, represented products are sold through industrial distribution and direct to large original equipment manufacturers." />

<title>Ware Industrial | Southeastern Engineered Product Representatives</title>

<?php include 'library/php/scripts_top.php'; ?>
<link rel="shortcut icon" href="library/images/favicon.ico">
<link rel="canonical" href="http://www.wareindustrial.com/" />

</head>

<body id="home">
  <?php include 'library/php/masthead.php'; ?>
<div id="content" class="content clearfix">
    <?php if( !$detect->isMobile() ){ // if its not mobile ?>
      <div class="LCol"><img src="library/images/home_images/nook-jack_183x183.png" alt="NOOK Industries, INC."><img src="library/images/home_images/rosta_183x183.jpg" alt="Rosta USA"><img src="library/images/home_images/stober_183x183.png" alt="Stober"><img src="library/images/home_images/nook-ball_183x183.png" alt="NOOK Industries, INC."><img src="library/images/home_images/garlock-seal_183x183.png" alt="Garlock an EnPro Industries family of companies"><img src="library/images/home_images/senqcia_183x183.jpg" alt="Senqcia Maxco, Ltd."><img src="library/images/home_images/sgi-power_183x183.png" alt="Nook"><img src="library/images/home_images/techtop_183x183.jpg" alt="TECHTOP"><img src="library/images/home_images/nexen_183x183.jpg" alt="Nexen"></div>
    <?php } elseif ( $detect->isMobile() && $detect->isTablet() ) { //if its mobile and if its a tablet ?> 
      <div class="LCol"><img src="library/images/home_images/nook-jack_183x183.png" alt="NOOK Industries, INC."><img src="library/images/home_images/rosta_183x183.jpg" alt="Rosta USA"><img src="library/images/home_images/stober_183x183.png" alt="Stober"><img src="library/images/home_images/nook-ball_183x183.png" alt="NOOK Industries, INC."><img src="library/images/home_images/garlock-seal_183x183.png" alt="Garlock an EnPro Industries family of companies"><img src="library/images/home_images/senqcia_183x183.jpg" alt="Senqcia Maxco, Ltd."><img src="library/images/home_images/pt-industrial_183x183.png" alt="Nook"><img src="library/images/home_images/techtop_183x183.jpg" alt="TECHTOP"><img src="library/images/home_images/nexen_183x183.jpg" alt="Nexen"></div>
    <?php } ?>

      <div class="copyScroll RCol">
      	<h1>Manufacturer&#8217;s Representative</h1>
        <p>Ware Industrial Sales is a manufacturer&#8217;s representative of engineered and industrial products including power transmission, bearings and linear motion. Serving the Southeastern United States, represented products are sold through industrial distribution and direct to large original equipment manufacturers. We provide technical support and a strategically located sales team to support our principals and customers in a professional manner.</p>

        <h2>Experience in Industrial Supply and Logistics</h2>
        <p>With over 50 years of industry experience and a track record of successful sales growth, our multi-man agency has an excellent reputation, driven with an in-depth knowledge of all aspects of industrial sales and operations.</p>

        <h2>Applied Knowledge across Product Lines</h2>
        <p>Ware Industrial employs a consultative sales approach derived from our broad based industry knowledge and experience.  We believe in controlling our own destiny by motivating product specification through direct sales calls.</p>
</div>
    </div>
<?php include 'library/php/footer.php'; ?>

<?php include 'library/php/scripts_btm.php'; ?>
<script src="library/js/eMail.js"></script>

</body>
</html>