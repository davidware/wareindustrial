<?php
require_once 'library/php/Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();
?>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="keywords"
          content="Ware Industrial, manufacturers representative, manufacturer's representative, southeast, power transmission sales, bearing sales, engineering class chain, linear bearings, variable frequency inverters"/>
    <meta name="description"
          content="Ware Industrial Sales has strategically located sales teams to provide effective coverage of the Southeast."/>

    <title>Ware Industrial | Southeastern Engineered Product Representatives</title>

    <?php include 'library/php/scripts_top.php'; ?>

    <link rel="shortcut icon" href="library/images/favicon.ico">
    <link rel="canonical" href="http://www.wareindustrial.com/"/>

</head>

<body id="territory">
<?php include 'library/php/masthead.php'; ?>
<div id="content" class="content clearfix">
    <?php if (!$detect->isMobile()) { // if its not mobile ?>
        <div class="paper LCol" id="paper">
            <?php include 'library/php/territory_map.php'; ?>
        </div>
    <?php } elseif ($detect->isMobile() && $detect->isTablet()) { //if its mobile and if its a tablet ?>
        <div class="paper LCol" id="paper">
            <?php include 'library/php/territory_map.php'; ?>
        </div>
    <?php } ?>
    <div class="RCol">
        <div id="location" class="clearfix">
            <h1>Strategically Located</h1>
            <p>Ware Industrial Sales has strategically located sales teams to provide effective coverage of the
                Southeast.</p>
            <p>This is a diverse market; we have 71 individual branch locations of multistate and independent
                distributors serving the MRO needs of some of the largest and best run fortune 500 companies. Industries
                we serve include: food and beverage, textiles, furniture, automotive, tire manufacturing, small machine
                fabrication, mining, aggregate, pulp and paper, steel, lumber, automation integration and
                pharmaceuticals.</p>
            <p>Original Equipment Manufacturers support regional, national and international customers in all aspects of
                industry, from agricultural and construction equipment to conveyors and packaging machinery.</p>
            <h3>Our Team</h3>
            <ul>
                <li class="devin">Devin Ware</li>
                <li class="steve">Steve Johnson</li>
                <li class="tom">Tom Muth</li>
                <li class="cody">Cody Muth</li>
            </ul>
            <?php
            //<img id="Charlotte" src="library/images/4344017_P_Charlotte-NC.jpeg" alt="Charlotte, NC: FPO" />
            ?>
        </div>
        <div id="devin" class="clearfix">
            <h2>Devin Ware</h2>
            <p>Devin Ware is Managing Member and Regional Sales Manager for Eastern Virginia and Upstate South Carolina .</p>
            <?php if ($detect->isMobile() && !$detect->isTablet()) { // if it is mobile but not a tablet ?>
                <p>You can reach Devin at <a href="tel:+17045250489">(704) 525-0489</a></p>
            <?php } else { ?>
                <p>You can reach Devin at (704) 525-0489</p>
            <?php } ?>
            <p>or e-mail: <a href="/devin|wareindustrial.com" class="email">Devin Ware</a></p>
            <p><a class="close">close</a></p>
        </div>
        <div id="steve" class="clearfix">
            <h2>Steve Johnson</h2>
            <p>Steve is Regional Sales Manager for central and Western North Carolina.
                <!-- Area codes 336, 704 and 828 --></p>
            <?php if ($detect->isMobile() && !$detect->isTablet()) { // if it is mobile but not a tablet ?>
                <p>You can reach Steve at <a href="tel:+17049627276">(704) 962-7276</a></p>
            <?php } else { ?>
                <p>You can reach Steve at (704) 962-7276</p>
            <?php } ?>
            <p>or e-mail: <a href="/steve|wareindustrial.com" class="email">Steve Johnson</a></p>
            <p><a class="close">close</a></p>
        </div>
        <div id="tom" class="clearfix">
            <h2>Tom Muth</h2>
            <p>Tom Muth is Regional Sales Manager for West Virginia, Western Virginia and Eastern Tennessee.</p>
            <?php if ($detect->isMobile() && !$detect->isTablet()) { // if it is mobile but not a tablet ?>
                <p>You can reach Tom at <a href="tel:+18652374943">(865) 237-4943</a></p>
            <?php } else { ?>
                <p>You can reach Tom at (865) 237-4943</p>
            <?php } ?>
            <p>or e-mail: <a href="/tom|wareindustrial.com" class="email">Tom Muth</a></p>
            <p><a class="close">close</a></p>
        </div>
        <div id="cody" class="clearfix">
            <h2>Cody Muth</h2>
            <p>Cody Muth is Regional Sales Manager for Eastern North Carolina and Eastern South Carolina.</p>
            <?php if ($detect->isMobile() && !$detect->isTablet()) { // if it is mobile but not a tablet ?>
                <p>You can reach Cody at <a href="tel:+18655858113">(865) 585-8113</a></p>
            <?php } else { ?>
                <p>You can reach Cody at (865) 585-8113</p>
            <?php } ?>
            <p>or e-mail: <a href="/cody|wareindustrial.com" class="email">Cody Muth</a></p>
            <p><a class="close">close</a></p>
        </div>
    </div>
</div>
<?php include 'library/php/footer.php'; ?>

<?php include 'library/php/scripts_btm.php'; ?>

<?php if ($detect->isMobile()) { // if it is mobile load the js ?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/hammer.js/1.0.5/hammer.min.js"></script>
    <script src="library/js/territory_m-ck.js"></script>
<?php } elseif ($detect->isMobile() && $detect->isTablet()) { //if it is mobile and is a tablet load the js ?>
    <script src="library/js/territory_m-ck.js"></script>
<?php } else { ?>
    <script src="library/js/territory-ck.js"></script>
<?php } ?>

</body>
</html>