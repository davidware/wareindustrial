<?php
require_once 'library/php/Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();
?>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
<meta charset="utf-8" />
<meta name="keywords" content="Ware Industrial, manufacturers representative, manufacturer's representative, southeast,  southeast manufacturer's representative, power transmission sales, bearing sales, engineering class chain, linear bearings, variable frequency inverters" />
<meta name="description" element="The Ware Industrial team has a broad knowledge of the local market.  Factory training in multiple product lines makes us an excellent resource for our customers." />

<title>Ware Industrial | Southeastern Engineered Product Representatives</title>

<?php include 'library/php/scripts_top.php'; ?>

<link rel="shortcut icon" href="library/images/favicon.ico">
<link rel="canonical" href="http://www.wareindustrial.com/" />

</head>

<body id="products">
<?php include 'library/php/masthead.php'; ?>
  <div id="lineCard" class="lineCard clearfix">
 <?php /*
    <div class="productLine grid-sizer">
      <img class="large" src="library/images/associate_images/product_bunting_465x450.jpg" alt="Bunting Bearings Corporation" />
      <h3>Bunting Bearings Corporation</h3>
      <p>Cast and powdered metal bronze plane bearings, engineered plastic bearings</p>
      <ul>
        <li>p: 888-286-8464</li>
        <li>f: 419-866-0653</li>
        <li><a href="http://www.buntingbearings.com/" target="_blank">www.buntingbearings.com</a></li>
      </ul>
    </div>
    
    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_centa_465x450.jpg" alt="Centa" />
      <h3>Centa</h3>
      <p>Whether calculating simple applications or complex systems: <br />
      20 series of flexible couplings, drive shafts and supplementary products with several variations offer optimum and uncompromising results. Low-cost, reliable and technically sophisticated. </p>
      <ul>
        <li>p: 630-236-3500</li>
        <li>f: 630-236-3565</li>
        <li><a href="http://www.centa.info" target="_blank">www.centa.info</a></li>
      </ul>
    </div>
    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_diequa_465x450.jpg" alt="Diequa Corporation" />
      <img class="thumb" src="library/images/associate_images/product_diequa_75x75.jpg" alt="Diequa Corporation" />
      <img class="thumb" src="library/images/associate_images/product_diequa1_75x75.jpg" alt="Diequa Corporation" />
      <img class="thumb" src="library/images/associate_images/product_diequa2_75x75.jpg" alt="Diequa Corporation" />
      <img class="thumb" src="library/images/associate_images/product_diequa3_75x75.jpg" alt="Diequa Corporation" />
      <h3>Diequa Corporation</h3>
      <p>Manufacturer of industrial gear drives and gear motors. High precision gear heads for servo applications. Inline or right angle configurations available. High precision spiral bevel gearboxes. Custom gear drives and specials.</p>
      <ul>
        <li>p: 847-678-3581</li>
        <li>f: 847-678-3583</li>
        <li><a href="http://www.diequa.com" target="_blank">www.diequa.com</a></li>
      </ul>
    </div>

    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_hilliard_465x450.jpg" alt="The Hilliard Corporation" />
      <h3>The Hilliard Corporation</h3>
      <p>Intermittent, slip, centrifugal, overrunning, bi-directional, disconnect, and single revolution clutches; torque limiters and electric disc brakes. TWIFLEX pneumatic, electric, hydraulic, mechanical, and spring applied caliper brakes for stopping, holding, and tensioning for industrial and marine applications.</p>
      <ul>
        <li>p: 607-733-7121</li>
        <li>f: 607-733-0928</li>
        <li><a href="http://www.hilliardcorp.com/" target="_blank">www.hilliardcorp.com</a></li>
      </ul>
    </div>

    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_hudson_465x450.jpg" alt="Hudson Bearings" />
      <h3>Hudson Bearings</h3>
      <p>Manufacturer of the highest quality cargo and material handling steel and nylon ball transfers and ball transfer units.</p>
      <ul>
        <li>p: 860-739-6988</li>
        <li>f: 860-739-7251</li>
        <li><a href="http://www.hudsonbearings.com/index.php" target="_blank">www.hudsonbearings.com</a></li>
      </ul>
    </div>
    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_invertek_465x450.jpg" alt="Invertek Drives" />
      <h3>Invertek</h3>
      <p>AC variable frequency drives, Fractional through 200 HP,  Infrared communication for quick, easy commissioning of drives. Available in 115v, 230v, 460v, and 575v.</p>
      <ul>
        <li>p: 314-614-4788</li>
        <li>f: 636-625-0980</li>
        <li><a href="http://www.invertek.co.uk/" target="_blank">www.invertek.co.uk</a></li>
      </ul>
    </div>

    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_mitshuboshi_465x450.jpg" alt="Mitshuboshi Belting Ltd." />
      <h3>Mitshuboshi Belting Ltd.</h3>
      <p>Global manufacturer of high quality belting products including conventional v-belts, narrow width v-belts, HTD timing belts, curvilinear synchronous belts; interchangeable with industry standard profiles.</p>
      <ul>
        <li>p: 877-625-7832</li>
        <li>f: 815-434-2897</li>
        <li><a href="http://www.mblusa.com" target="_blank">www.mblusa.com</a></li>
      </ul>
    </div>
 
    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_nexen_465x450.jpg" alt="Nexen Group" />
      <div id="tabContainer">
		<ul class="tabs">
			<li id="tabHeader_1">Precision Motion Control</li>
			<li id="tabHeader_2">Air Champ Clutches &amp; Breaks</li>
			<li id="tabHeader_3">Web Tension Brakes &amp; Controls</li>
		</ul>
		<div class="tab" id="tab_1">
			<img class="thumb" src="library/images/associate_images/product_nexen1_75x75.jpg" alt="Nexen Group: Precision Motion Control" />
			<img class="thumb" src="library/images/associate_images/product_nexen2_75x75.jpg" alt="Nexen Group: Precision Motion Control" />
			<img class="thumb" src="library/images/associate_images/product_nexen3_75x75.jpg" alt="Nexen Group: Precision Motion Control" />
			<img class="thumb" src="library/images/associate_images/product_nexen4_75x75.jpg" alt="Nexen Group: Precision Motion Control" />
			<h3>Nexen Group</h3>
      		<h4>Precision Motion Control</h4>
			
			<h5>RPS High Performance Linear Drive System</h5>
			<p>Take all the things you like about ball screws and rack and pinion and eliminate all their short comings what do you get? The RPS System!</p>
			<ul>
				<li>Positional accuracy up to ±20um</li>
				<li>Backlash less than 3.2um</li>
				<li>Speeds up to 11 m/s</li>
				<li>Unlimited run lengths</li>
				<li>Long life, low maintenance</li>
				<li>99% efficient, quieter, low particle emissions</li>
			</ul>
			<h5>RPG High Performance Rotary Drive System</h5>
			<p>Overcome the shortcomings of chain, belt, and traditional gear drives.</p>
			<ul>
				<li>Tangential accuracy up to ±30um at MPC</li>
				<li>Tangential backlash less than 3.2um at MPC</li>
				<li>High speeds</li>
				<li>Unlimited gear diameters and arc radiuses</li>
				<li>Long life, low maintenance</li>
				<li>99% efficient, quieter, low particle emissions</li>
			</ul>
			<h5>RB, RBR, and RBE Profile Guide Rail Brakes</h5>
			<p>Nexen rail brakes clamp directly on profile guide rails to hold or E-stop loads.</p>
			<ul>
				<li>Spring engaged for fail safe holding or E-stops</li>
				<li>Pneumatic and electrically released models</li>
				<li>Provides low backlash and high load stiffness</li>
				<li>Ideal for redundant servo brakemotor braking</li>
				<li>Does not contact rail bearing surfaces or effect life</li>
				<li>Large adjustable brake shoes for low wear</li>
				<li>Similar geometry to the bearing cassettes</li>
				<li>No maintenance</li>
				<li>Available in air or electrically released models</li>
				<li>Models to fit most manufacturers and rail sizes</li>
				<li>Braking at the load eliminates loss of control due to power train failures</li>
			</ul>
			<h5>RL Rod Locks</h5>
			<p>Nexen rod locks clamp directly on pneumatic cylinder rods or precision shafting to hold or E-stop loads.</p>
			<ul>
				<li>Spring engaged for fail safe holding or infrequent E-stops</li>
				<li>Provides low backlash and high load stiffness</li>
				<li>Ideal for redundant servo brakemotor braking</li>
				<li>Large clamping area for low wear and consistent holding performance</li>
				<li>Models to fit most cylinders and shaft sizes</li>
				<li>NFPA and ISO models</li>
				<li>Optional manual release and locked mode feedback</li>
				<li>Models with IP67, NEMA 4X ratings</li>
				<li>No maintenance</li>
			</ul>
			</p>
		</div>
		<div class="tab" id="tab_2">
			<img class="thumb" src="library/images/associate_images/product_nexen5_75x75.jpg" alt="Nexen Group: Air Champ Clutches &amp; Breaks" />
			<img class="thumb" src="library/images/associate_images/product_nexen5_75x75.jpg" alt="Nexen Group: Air Champ Clutches &amp; Breaks" />
			<img class="thumb" src="library/images/associate_images/product_nexen5_75x75.jpg" alt="Nexen Group: Air Champ Clutches &amp; Breaks" />
			<h3>Nexen Group</h3>
      		<h4>Air Champ Clutches &amp; Breaks</h4>
      		<p></p>
		</div>
		<div class="tab" id="tab_3">
			<img class="thumb" src="library/images/associate_images/product_nexen6_75x75.jpg" alt="Nexen Group: Web Tension Brakes &amp; Controls" />
			<img class="thumb" src="library/images/associate_images/product_nexen6_75x75.jpg" alt="Nexen Group: Web Tension Brakes &amp; Controls" />
			<img class="thumb" src="library/images/associate_images/product_nexen6_75x75.jpg" alt="Nexen Group: Web Tension Brakes &amp; Controls" />
			<h3>Nexen Group</h3>
      		<h4>Web Tension Brakes &amp; Controls</h4>
      		<p></p>
		</div>
      </div>
      
      <p>Nexen offers an advanced technology that revolutionizes linear motion.</p>
      <p>The Roller Pinion System (RPS) provides zero backlash, very high positional accuracy, unlimited length, very high speeds, 99% efficiency, high rigidity, low noise, low maintenance, corrosion resistance, and long life.</p>
      <ul>
        <li>p: 800-843-7445</li>
        <li>f: 651-286-1099</li>
        <li><a href="http://www.nexengroup.com/nexen/index.jsp" target="_blank">www.nexengroup.com</a></li>
      </ul>
    </div>
    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_nook_465x450.jpg" alt="Nook Industries" />
      <h3>Nook Industries</h3>
      <p>Manufacturer&#8217;s inch &amp; metric mechanical &amp; electro-mechanical actuators, ball screws, acme screws, roller screws and assemblies, worm gear screw jacks, and electrical cylinders. Profile guide rail, shafting and bearings</p>
      <ul>
        <li>p: 800-321-7800</li>
        <li>f: 216-271-7020</li>
        <li><a href="http://www.nookindustries.com/" target="_blank">www.nookindustries.com</a></li>
      </ul>
    </div>
    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_orthman_465x450.jpg" alt="Orthman Conveyor" />
      <h3>Orthman Conveyor</h3>
      <p>Screw Conveyors, Bucket Elevators and bulk handling systems. Carbon and Stainless Steel, CEMA standard and special designs. Ruggedly build for reliable service.</p>
      <ul>
        <li>p: 800-642-9551</li>
        <li>f: 573-443-2413</li>
        <li><a href="http://www.conveyusa.com/" target="_blank"> www.conveyusa.com</a></li>
      </ul>
    </div>
    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_osborn_465x450.jpg" alt="Osborn-Load Runners" />
      <h3>Osborn-Load Runners</h3>
      <p>Specially designed idler-rollers tolerate both radial and thrust loads for heavy-duty applications </p>
      <ul>
        <li>p: 216-361-1900</li>
        <li>f: 216-361-1913</li>
        <li><a href="http://www.loadrunners.com/" target="_blank">www.loadrunners.com</a></li>
      </ul>
    </div>
    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_peer_465x450.jpg" alt="Peer Chain" />
      <h3>Peer Chain</h3>
      <p>From construction to agricultural equipment to microchip conveyors, Peer Chain offers assembly and flexibility to meet a variety of industrial applications. Roller chain to engineered class chain and specialty attachment and coated chain.</p>
      <ul>
        <li>p: 800-523-4567</li>
        <li>f: 847-775-4650</li>
        <li><a href="http://www.peerchain.com/" target="_blank">www.peerchain.com</a></li>
      </ul>
    </div>

    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_sci-couplings_465x450.jpg" alt="System Components, Inc." />
      <h3>System Components, Inc.</h3>
      <p>Custom &amp; Standard Flanged, Sleeve &amp; Elastomeric Couplings in stock sizes from 1-F through 10-F for bore sizes up to 14-1/8". Custom sizes are available up to 30-F for bore sizes up to 46" diameter. Continuous sleeve couplings in stock sizes from 6-S through 45- S. Non stock couplings can be provided for bore sizes up to 15".</p>
      <ul>
        <li>p: 800-866-1290</li>
        <li>f: 269-637-8377</li>
        <li><a href="http://www.sci-couplings.com/" target="_blank">www.sci-couplings.com</a></li>
      </ul>
    </div>

    <div class="productLine">
      <img class="large" src="library/images/associate_images/product_voith_465x450.jpg" alt="Voith" />
      <h3>Voith</h3>
      <p>Fluid couplings and start up components, large Cardan Shafts/Universal Joints and Safeset Torque limiting couplings. One of the oldest and most relied upon Global manufacturer of power transmission components.</p>
      <ul>
        <li>p: 717-767-3200</li>
        <li>f: 717-767-3210</li>
        <li><a href="http://www.us.voithturbo.com/index_e.php" target="_blank">www.us.voithturbo.com</a></li>
      </ul>
    </div>
      
   --> */
   ?>
  </div>

<?php include 'library/php/footer.php'; ?>

<?php include 'library/php/scripts_btm.php'; ?>
<script src="library/js/masonry.pkgd-ck.js"></script>
<script src="library/js/products-ck.js"></script>
  
</body>
</html>