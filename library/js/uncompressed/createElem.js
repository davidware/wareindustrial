document.createElement('header');
document.createElement('nav');
document.createElement('section');
document.createElement('article');
document.createElement('footer');

$(function(){
	// set the newly created elements to display block
	$('header, nav, section, article, footer').css('display', 'block');
	$('footer').addClass('footer');

	// Since this is only on IE 8 or lower add an image for the territory map because the svg does not display
	$('#paper').html('<img src="library/images/territory.png" alt="Ware Industrial&#8217s territory" height="677" width="550" />');
});