// JavaScript Document 09-27-2013
// 
// 7 column: 952px / 7 = 136
// 

var pLines = '';

$(document).ready(function(){
	$('#lineCard').ready(function(){
		$.ajaxSetup ({ cache: false });
		$.getJSON('library/js/products.json', function(json){
			// console.log(json);
			pLines = json;
			populateProductLines(pLines);
		});
	});
	
	// write the html to the page
	function populateProductLines(pLines) {
		var lineHTML = '';
		$.each(pLines.productLines, function (i, productLines) {
			//console.debug('there are ' + pLines.productLines.length + ' lines');
			lineHTML = '<div class="productLine">';
			lineHTML = lineHTML +  '<img class="large" src="' + pLines.productLines[i].largeImage + '" alt="' + pLines.productLines[i].largeImageAlt + '" />';
			lineHTML = lineHTML + '<h4>' + pLines.productLines[i].largeImageAlt + '</h4>';
			if(productLines.thumbImage != null){
				$.each(productLines.thumbImage, function (i, thumbImage) {
					lineHTML = lineHTML + '<img class="thumb" src="' + productLines.thumbImage[i].thumbPath + '" title="' + productLines.thumbImage[i].thumbAlt + '" alt="' + productLines.thumbImage[i].thumbAlt + '" />';
                });
			}
			lineHTML = lineHTML + '<h3>' + pLines.productLines[i].line + '</h3>';
			$.each(productLines.description, function (i, description) {
				lineHTML = lineHTML + '<p>' + productLines.description[i] + '</p>';
			});
			lineHTML = lineHTML + '<ul>';
			lineHTML = lineHTML + '<li>p: ' + pLines.productLines[i].phone + '</li>';
			if(pLines.productLines[i].fax != null && pLines.productLines[i].fax.length > 0){
				//console.debug(pLines.productLines[i].fax.length > 0);
				lineHTML = lineHTML + '<li>f: ' + pLines.productLines[i].fax + '</li>';
			}
			lineHTML = lineHTML + '<li><a href="' + pLines.productLines[i].url + '" target="_blank">' + pLines.productLines[i].urlText + '</a></li>';
			lineHTML = lineHTML + '</ul>';
			lineHTML = lineHTML + '</div>';

			$(lineHTML).appendTo("#lineCard");
		});
		// to use ems:  var em = parseFloat( $container.css('font-size') );
		// initialize Masonry
		var container = $('#lineCard').imagesLoaded( function() {
			container.masonry({
				columnWidth: 136, //'.grid-sizer',
				itemSelector: '.productLine',
				containerStyle: null
				//gutter: 16
			});
		});
		// click to add the class 'width3' to the div; remove from all others
		$('.productLine').on('click', function(){
			//console.log('clicked');
			$('.productLine').removeClass('width3');//, 1000, 'easeOutQuad');
			$(this).toggleClass('width3');//, 1000, 'easeOutQuad');
			// trigger layout
			container.masonry();
		});
		$('.thumb').ready(function(){
		    // click to open the large version of the thumb
			$('.thumb', this).click(function(){
				// console.log(this);
				var thmb = $(this);
				// console.log(thmb);
				var img = $(this).siblings('.large');
				var caption = $(this).siblings('h4');
				// console.log(img, img.attr('src'));
				img.attr('src',thmb.attr('src').replace('75x75','465x450'));
				caption.html(thmb.attr('title'));
			});
		});
	}
}); // close ready

	
