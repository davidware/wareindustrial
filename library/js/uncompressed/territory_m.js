// JavaScript Document
// Create the Territory map SVG using SVG.js
$(function(){

// #West_Virginia -> Devin
// #South_Carolina -> Warren
// #North_Carloina_Eastern -> Steve
// #North_Carolina_Western -> Devin
// #Virginia -> Devin
// 

// set the initial state 
$('#location').show();
$('#devin, #steve, #warren').hide();

// On Click show the appropriate div
$('.devin, .steve, .warren').on('click', function(){
     //console.log($(this).hasClass('devin'));
        if ($(this).hasClass('devin')) {
             //console.log('c1');
            $('#location, #steve, #warren').hide();
            $('#devin').show();
        } else if ($(this).hasClass('steve')){
             //console.log('c2');
            $('#location, #devin, #warren').hide();
            $('#steve').show();
        } else if ($(this).hasClass('warren')){
             //console.log('c3');
            $('#location, #devin, #steve').hide();
            $('#warren').show();
        }
    });

// Looks like the SVG map requires ID; class doesn't work On Click show the appropriate div
$('#territory_1, #territory_2, #territory_3').on('click', function(){
    // console.log($(this).attr('id'));
        if ($(this).attr('id') === 'territory_1') {
            // console.log('c1');
            $('#location, #steve, #warren').hide();
            $('#devin').show();
        } else if ($(this).attr('id') === 'territory_3'){
            // console.log('c2');
            $('#location, #devin, #warren').hide();
            $('#steve').show();
        } else if ($(this).attr('id') === 'territory_2'){
            // console.log('c3');
            $('#location, #devin, #steve').hide();
            $('#warren').show();
        }
    });

    $('.close').on('click', function(){
        $('#location').show();
        $('#devin, #steve, #warren, #tom').hide();
    });
});