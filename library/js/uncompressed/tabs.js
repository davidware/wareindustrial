(function($){
	$.fn.formatTabs = function(options) {
		var settings = {
			'style' : 'default'
		};

		options = $.extend( settings, options );
		
		// style class will be added to the tab container
		return this.each (function () {
			var o = options;
			container = this;
			container.setAttribute("class",o.style);
			var navitem = container.querySelector("li");

			//store which tab we are on
			var ident = navitem.id.split("_")[1];
			navitem.parentNode.setAttribute("data-current",ident);

			//set current tab with class of activetabheader
			navitem.setAttribute("class","activeTab");

			//hide two tab contents we don't need
			var pages = container.querySelectorAll(".tab");
			for (var i = 1; i < pages.length; i++) {
				pages[i].style.display="none";
			}

			//this adds click event to tabs (changed var i to j if it breaks)
			var tabs = container.querySelectorAll("li");
			for (var j = 0; j < tabs.length; j++) {
				tabs[j].onclick=displayTab;
			}
		});

		// on click of one of tabs
		function displayTab() {
			var current = this.parentNode.getAttribute("data-current");

			//remove class of activetabheader and hide old contents
			document.getElementById("tabHead_" + current).removeAttribute("class");
			document.getElementById("tab_" + current).style.display="none";

			var ident = this.id.split("_")[1];

			//add class of activetabheader to new active tab and show contents
			this.setAttribute("class","activeTab");
			document.getElementById("tab_" + ident).style.display="block";
			this.parentNode.setAttribute("data-current",ident);
		}
	};
})(window.jQuery || window.Zepto);