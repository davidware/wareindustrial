// JavaScript Document
$(function(){
	// set the initial state 
	$('#location').show();
	$('#devin, #steve, #warren, #tom').hide();

	function switchDiv (territory) {
		switch (territory) {
			case 'devin':
				$('#devin').show().siblings().hide();
				break;
			case 'steve':
				$('#steve').show().siblings().hide();
				break;
			case 'warren':
				$('#warren').show().siblings().hide();
				break;
			case 'tom':
				$('#tom').show().siblings().hide();
				break;
			default:
				$('#location').show().siblings().hide();
		}
	}

	// On Click on one of the territories on the map to show the appropriate div and add the active class
	$('.devin, .steve, .warren, .tom').on('click', function(){
		var territory = $(this).attr('class'),
			panel = $('#devin') || $('#steve') || $('#warren') || $('#tom') || $('#location');

		panel.addClass('active');
		switchDiv (territory);
	});

	// On Mouseover show the div
	$('svg .devin, svg .steve, svg .warren, svg .tom').on('mouseover', function(){
		var territory = $(this).attr('class');
		switchDiv (territory);
	});

	// On Mouseout hide the div
	$('svg .devin, svg .steve, svg .warren, svg .tom').on('mouseout', function(){
		var territory = $(this).attr('class'),
			panel = $('#devin') || $('#steve') || $('#warren') || $('#tom') || $('#location');

		if ( !panel.hasClass('active') ) {
			$('#location').show().siblings().hide();
		}
	});

	$('.close').on('click', function(){
		var panel = $('#devin') || $('#steve') || $('#warren') || $('#tom') || $('#location');
		$('#location').show().siblings().hide();
		panel.removeClass('active');
	});
});