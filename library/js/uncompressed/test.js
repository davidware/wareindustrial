
// valid JSON file
var json_data = {
    "Lions": [{
        "Title": "Lion 1",
            "Image": "http://creative.nci.com/frontendevtest/lion-01.jpg"
    }, {
        "Title": "Lion 2",
            "Image": "http://creative.nci.com/frontendevtest/lion-02.jpg"
    }],
        "Tiger": [{
        "Title": "Tiger 1",
            "Image": "http://creative.nci.com/frontendevtest/tiger-01.jpg"
    }, {
        "Title": "Tiger 2",
            "Image": "http://creative.nci.com/frontendevtest/tiger-02.jpg"
    }],
        "Bears": [{
        "Title": "Bear 1",
            "Image": "http://creative.nci.com/frontendevtest/bear-01.jpg"
    }, {
        "Title": "Bear 2",
            "Image": "http://creative.nci.com/frontendevtest/bear-02.jpg"
    }]
};

var animal_list = jQuery.parseJSON('json_data');
/**/
jQuery(document).ready(function() {
    
    jQuery('.dave').append(' <i>dave</i>');

    for (var i = 0; i < animal_list.length; i++) {

        var option = $("<option>");

        for (var key in animal_list[i]) {
            // There should only be two keys, if its a number its ID, else option name
            if (typeof animal_list[i][key] === "number") {
                option.attr("value", animal_list[i][key]);
            } else {
                option.html(animal_list[i][key]);
            }
        }
        jQuery("select").append(option);
    }
});