/* 
 * The MIT License
 *
 * Copyright (c) 2012 James Allardice
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */// Defines the global Placeholders object along with various utility methods
(function(e) {
    "use strict";
    function t(e, t, n) {
        if (e.addEventListener) return e.addEventListener(t, n, !1);
        if (e.attachEvent) return e.attachEvent("on" + t, n);
    }
    function n(e, t) {
        var n, r;
        for (n = 0, r = e.length; n < r; n++) if (e[n] === t) return !0;
        return !1;
    }
    function r(e, t) {
        var n;
        if (e.createTextRange) {
            n = e.createTextRange();
            n.move("character", t);
            n.select();
        } else if (e.selectionStart) {
            e.focus();
            e.setSelectionRange(t, t);
        }
    }
    function i(e, t) {
        try {
            e.type = t;
            return !0;
        } catch (n) {
            return !1;
        }
    }
    e.Placeholders = {
        Utils: {
            addEventListener: t,
            inArray: n,
            moveCaret: r,
            changeType: i
        }
    };
})(this);

(function(e) {
    "use strict";
    function _() {}
    function D() {
        try {
            return document.activeElement;
        } catch (e) {}
    }
    function P(e, t) {
        var n, r, i = !!t && e.value !== t, o = e.value === e.getAttribute(a);
        if ((i || o) && e.getAttribute(f) === "true") {
            e.removeAttribute(f);
            e.value = e.value.replace(e.getAttribute(a), "");
            e.className = e.className.replace(s, "");
            r = e.getAttribute(v);
            if (parseInt(r, 10) >= 0) {
                e.setAttribute("maxLength", r);
                e.removeAttribute(v);
            }
            n = e.getAttribute(l);
            n && (e.type = n);
            return !0;
        }
        return !1;
    }
    function H(e) {
        var t, n, r = e.getAttribute(a);
        if (e.value === "" && r) {
            e.setAttribute(f, "true");
            e.value = r;
            e.className += " " + i;
            n = e.getAttribute(v);
            if (!n) {
                e.setAttribute(v, e.maxLength);
                e.removeAttribute("maxLength");
            }
            t = e.getAttribute(l);
            t ? e.type = "text" : e.type === "password" && w.changeType(e, "text") && e.setAttribute(l, "password");
            return !0;
        }
        return !1;
    }
    function B(e, t) {
        var n, r, i, s, f, l, c;
        if (e && e.getAttribute(a)) t(e); else {
            i = e ? e.getElementsByTagName("input") : o;
            s = e ? e.getElementsByTagName("textarea") : u;
            n = i ? i.length : 0;
            r = s ? s.length : 0;
            for (c = 0, l = n + r; c < l; c++) {
                f = c < n ? i[c] : s[c - n];
                t(f);
            }
        }
    }
    function j(e) {
        B(e, P);
    }
    function F(e) {
        B(e, H);
    }
    function I(e) {
        return function() {
            E && e.value === e.getAttribute(a) && e.getAttribute(f) === "true" ? w.moveCaret(e, 0) : P(e);
        };
    }
    function q(e) {
        return function() {
            H(e);
        };
    }
    function R(e) {
        return function(t) {
            x = e.value;
            if (e.getAttribute(f) === "true" && x === e.getAttribute(a) && w.inArray(n, t.keyCode)) {
                t.preventDefault && t.preventDefault();
                return !1;
            }
        };
    }
    function U(e) {
        return function() {
            P(e, x);
            if (e.value === "") {
                e.blur();
                w.moveCaret(e, 0);
            }
        };
    }
    function z(e) {
        return function() {
            e === D() && e.value === e.getAttribute(a) && e.getAttribute(f) === "true" && w.moveCaret(e, 0);
        };
    }
    function W(e) {
        return function() {
            j(e);
        };
    }
    function X(e) {
        if (e.form) {
            L = e.form;
            typeof L == "string" && (L = document.getElementById(L));
            if (!L.getAttribute(c)) {
                w.addEventListener(L, "submit", W(L));
                L.setAttribute(c, "true");
            }
        }
        w.addEventListener(e, "focus", I(e));
        w.addEventListener(e, "blur", q(e));
        if (E) {
            w.addEventListener(e, "keydown", R(e));
            w.addEventListener(e, "keyup", U(e));
            w.addEventListener(e, "click", z(e));
        }
        e.setAttribute(h, "true");
        e.setAttribute(a, C);
        (E || e !== D()) && H(e);
    }
    var t = [ "text", "search", "url", "tel", "email", "password", "number", "textarea" ], n = [ 27, 33, 34, 35, 36, 37, 38, 39, 40, 8, 46 ], r = "#ccc", i = "placeholdersjs", s = new RegExp("(?:^|\\s)" + i + "(?!\\S)"), o, u, a = "data-placeholder-value", f = "data-placeholder-active", l = "data-placeholder-type", c = "data-placeholder-submit", h = "data-placeholder-bound", p = "data-placeholder-focus", d = "data-placeholder-live", v = "data-placeholder-maxlength", m = document.createElement("input"), g = document.getElementsByTagName("head")[0], y = document.documentElement, b = e.Placeholders, w = b.Utils, E, S, x, T, N, C, k, L, A, O, M;
    b.nativeSupport = m.placeholder !== void 0;
    if (!b.nativeSupport) {
        o = document.getElementsByTagName("input");
        u = document.getElementsByTagName("textarea");
        E = y.getAttribute(p) === "false";
        S = y.getAttribute(d) !== "false";
        T = document.createElement("style");
        T.type = "text/css";
        N = document.createTextNode("." + i + " { color:" + r + "; }");
        T.styleSheet ? T.styleSheet.cssText = N.nodeValue : T.appendChild(N);
        g.insertBefore(T, g.firstChild);
        for (M = 0, O = o.length + u.length; M < O; M++) {
            A = M < o.length ? o[M] : u[M - o.length];
            C = A.attributes.placeholder;
            if (C) {
                C = C.nodeValue;
                C && w.inArray(t, A.type) && X(A);
            }
        }
        k = setInterval(function() {
            for (M = 0, O = o.length + u.length; M < O; M++) {
                A = M < o.length ? o[M] : u[M - o.length];
                C = A.attributes.placeholder;
                if (C) {
                    C = C.nodeValue;
                    if (C && w.inArray(t, A.type)) {
                        A.getAttribute(h) || X(A);
                        if (C !== A.getAttribute(a) || A.type === "password" && !A.getAttribute(l)) {
                            A.type === "password" && !A.getAttribute(l) && w.changeType(A, "text") && A.setAttribute(l, "password");
                            A.value === A.getAttribute(a) && (A.value = C);
                            A.setAttribute(a, C);
                        }
                    }
                } else if (A.getAttribute(f)) {
                    P(A);
                    A.removeAttribute(a);
                }
            }
            S || clearInterval(k);
        }, 100);
    }
    w.addEventListener(e, "beforeunload", function() {
        b.disable();
    });
    b.disable = b.nativeSupport ? _ : j;
    b.enable = b.nativeSupport ? _ : F;
})(this);