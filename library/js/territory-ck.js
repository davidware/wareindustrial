// JavaScript Document
$(function () {
    function e(e) {
        switch (e) {
            case "devin":
                $("#devin").show().siblings().hide();
                break;
            case "steve":
                $("#steve").show().siblings().hide();
                break;
            case "tom":
                $("#tom").show().siblings().hide();
                break;
            case "cody":
                $("#cody").show().siblings().hide();
                break;
            default:
                $("#location").show().siblings().hide();
        }
    }

    $("#location").show();
    $("#devin, #steve, #cody, #tom").hide();
    $(".devin, .steve, .cody, .tom").on("click", function () {
        var t = $(this).attr("class"), n = $("#devin") || $("#steve") || $("#cody") || $("#tom") || $("#location");
        n.addClass("active");
        e(t);
    });
    // $("svg .devin, svg .steve, svg .cody, svg .tom").on("mouseover", function () {
    //     var t = $(this).attr("class");
    //     e(t);
    // });
    // $("svg .devin, svg .steve, svg .cody, svg .tom").on("mouseout", function () {
    //     var e = $(this).attr("class"), t = $("#devin") || $("#steve") || $("#cody") || $("#tom") || $("#location");
    //     t.hasClass("active") || $("#location").show().siblings().hide();
    // });
    $(".close").on("click", function () {
        var e = $("#devin") || $("#steve") || $("#cody") || $("#tom") || $("#location");
        $("#location").show().siblings().hide();
        e.removeClass("active");
    });
});

$(function () {
    $(".email").each(function () {
        $(this).attr("href", $(this).attr("href").replace("|", "@").replace("/", "mailto:"));
    });
});