// JavaScript Document
// Create the Territory map SVG using SVG.js
$(function() {
    $("#location").show();
    $("#devin, #steve, #warren").hide();
    $(".devin, .steve, .warren").on("click", function() {
        if ($(this).hasClass("devin")) {
            $("#location, #steve, #warren").hide();
            $("#devin").show();
        } else if ($(this).hasClass("steve")) {
            $("#location, #devin, #warren").hide();
            $("#steve").show();
        } else if ($(this).hasClass("warren")) {
            $("#location, #devin, #steve").hide();
            $("#warren").show();
        }
    });
    $("#territory_1, #territory_2, #territory_3").on("click", function() {
        if ($(this).attr("id") === "territory_1") {
            $("#location, #steve, #warren").hide();
            $("#devin").show();
        } else if ($(this).attr("id") === "territory_3") {
            $("#location, #devin, #warren").hide();
            $("#steve").show();
        } else if ($(this).attr("id") === "territory_2") {
            $("#location, #devin, #steve").hide();
            $("#warren").show();
        }
    });
    $(".close").on("click", function() {
        $("#location").show();
        $("#devin, #steve, #warren, #tom").hide();
    });
});

$(function() {
    $(".email").each(function() {
        $(this).attr("href", $(this).attr("href").replace("|", "@").replace("/", "mailto:"));
    });
});