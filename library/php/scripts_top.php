<link href='//fonts.googleapis.com/css?family=GFS+Didot' rel='stylesheet'>
<link href='//fonts.googleapis.com/css?family=Cardo:400,700,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>

<link href="library/css/base.css" rel="stylesheet" />
<?php if( $detect->isMobile() ){ // Load CSS override for mobile ?>
<link href="library/css/mobile.css" rel="stylesheet" />
<?php } ?>


<!--[if lt IE 9]>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!--<![endif]-->

<!--[if lt IE 9]>
   <script src="library/js/createElem-ck.js"></script>
<![endif]-->
    
    
<script src="library/js/modernizr.custom.62883-ck.js"></script>

<?php /*
	<!-- Used this one to write in Zepto or JQuery
	<script>
	document.write('<script src="' + ('__proto__' in {} ? '//cdnjs.cloudflare.com/ajax/libs/zepto/1.0/zepto.min' : '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min') + '.js"><\/script>')
	</script> -->
*/ ?>


