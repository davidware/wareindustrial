 <?php
	if(isset($_POST['submit'])){
	    require_once('recaptchalib.php');
	    $privatekey = "6LeOy_sSAAAAAG_QHcydDVnQrXCjxNBhdRNFXqP0";
	    $resp = recaptcha_check_answer ($privatekey,
	                               		$_SERVER["REMOTE_ADDR"],
	                               		$_POST["recaptcha_challenge_field"],
	                                	$_POST["recaptcha_response_field"]);
	
	if (!$resp->is_valid) {
	    // What happens when the CAPTCHA was entered incorrectly
	    //header('Location: '.$exit);
	    //die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." ."(reCAPTCHA said: " . $resp->error . ")");
	    //$error = $resp->error;
	    echo "The reCAPTCHA wasn't entered correctly. Go back and try it again.";
	    // exit;
	} else {
	  	if(!empty($_POST['from_email'])){
	  		$exit = "http://dev.wareindustrial.com/contact_us.php";
	  	  //$exit = "http://www.wareindustrial.com/contact_us.php;
	  		
			// validation
	  		// if the email field is not empty die
	  		if(!empty($_POST['email'])){
	  			header('Location: '.$exit.'?error=efbot');
	  			exit;
	  		};
	  		// Do we have the required fields (js should catch these) otherwise die
	  		if(!isset($_POST['first_name']) ||
	  		!isset($_POST['last_name']) ||
	  		!isset($_POST['from_email'])) {
	  			header('Location: '.$exit.'?error=missing');
	  			exit;
	  		}
	  		// loop through the post array and if there are any line breaks die
	  		foreach ($_POST as $key => $value) {
	  			if(!preg_match('/recaptcha/',$key)){
	  				if (eregi("%0A",$key) 
	  				 || eregi("%0D",$key) 
	  				 || eregi("\\r",$key) 
	  				 || eregi("\\n",$key)){
	  					header('Location: '.$exit.'?error=1');
	  					exit;
	  				}
	  			}
	  		} // end validation
	  		
	  		//create the vars for the form fields
	  		$to = "dcware@earthlink.net";
	  		$subject = "A new contact e-mail from wareindustrial.com";
	  		$headers = "From: " . $from;
	  		$fName =   filter_var($_POST['first_name'], FILTER_SANITIZE_STRING);
	  		$lName =   filter_var($_POST['last_name'], FILTER_SANITIZE_STRING);
	  		$from =    filter_var($_POST['from_email'], FILTER_SANITIZE_EMAIL);
	  		$company = filter_var($_POST['company'], FILTER_SANITIZE_STRING);
	  		$phone =   filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
	  		$message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
	  		$new = "\n\r";

	  		// Create the date Timestamp
	  		$today = getdate();
	  		$date = $today[month]. ' '. $today[mday]. ', '. $today[year];
	  		
	  		//creat the e-mail message; date name company email phone
	  		$msg = $date.$new.$new;
	  		$msg .= $fName.' '.$lName.$new;
	  		$msg .= $company.$new;
	  		$msg .= $from.$new;
	  		$msg .= $phone.$new.$new.$new;
	  		$msg .= $message.$new;
	  		
	  		//echo $msg;
	  		
			mail($to, $subject, $msg, $headers);
	  		header ('Location: '.$exit);
	  		
	  		//echo 'success 8 ';
	  		
	  	} // end if( !empty(POST['from_email']))
	  } // end recaptcha else
 	} //end if (isset(POST['submit']))
  ?>