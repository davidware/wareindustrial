	<footer>
      <div class="foot1 vcard">
           <!-- <a href="http://wareindustrial.com/" class="url">www.wareindustrial.com</a> -->
      	<ul>
        	<li class="fn org">Ware Industrial</li>
          <?php //<li>&#8226;</li> ?>
          <li  class="adr"><span class="locality">Charlotte</span>, <span class="region">NC</span>
          <span class="geo">
              <span class="latitude">
                 <span class="value-title" title="35.2270869"></span>
              </span>
              <span class="longitude">
                 <span class="value-title" title="-80.8431267"></span>
              </span>
           </span>
          </li>
          <?php if( $detect->isMobile() && !$detect->isTablet() ){ // if it is mobile but not a tablet ?>
		  <li class="tel"><span class="type none">work</span>p: <span class="value">+1.704.525.0489</span></li>
          <li class="tel"><span class="type none">fax</span>f: <span class="value">+1.704.943.1553</span></li>
		  <?php } else { ?>
		  <li class="tel"><span class="type none">work</span>p: <span class="value">+1.704.525.0489</span></li>
          <li class="tel"><span class="type none">fax</span>f: <span class="value">+1.704.943.1553</span></li>
		  <?php } ?>
          <!-- <li><a href="contact_us.php">Contact Ware Industrial</a></li> -->
         <!--  <li>e-mail: <span title="info|wareindustrial.com" class="email">Ware Industrial</span></li> -->
          <li><a href="http://h2vx.com/vcf/wareindustrial.com/">Add to Address Book</a></li>
        </ul>
        
        </div>
      <div class="foot2">&#169; 2008 - <?php echo date('Y'); ?> Ware Industrial. All Rights Reserved.</div>
  </footer>
</div>