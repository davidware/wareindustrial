  <div class="wrapper">
    <div id="mastHead" class="mastHead clearfix">
      <div class="logo"><a href="index.php"><img src="library/images/wareIndustrial_logo.gif" alt="Ware Industrial" /></a></div>
      <div class="certification">
        <a class="ptra" href="http://www.ptra.org/" target="_blank"><img src="library/images/ptra_logo.gif" alt="Power-Motion Technology Representatives Association (PTRA)" /></a>
        <a class="cpmr" href="http://www.mrerf.org/cpmr/" target="_blank"><img src="library/images/cpmr_logo.gif" alt="Certified Professional Manufacturers Representative (CPMR)" /></a>
        <a class="mana"  href="http://www.manaonline.org/" target="_blank"><img src="library/images/mana_logo.gif" alt="Manufacturers' Agents National Association (MANA)" /></a>
        <span class="linkedin">
          <script src="//platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
          <script type="IN/FollowCompany" data-id="2953406" data-counter="none"></script>
        </span>
      </div>
    </div>
    <nav>
     <ul>
        <li><a href="index.php" id="nav_home">Home</a></li>
        <li><a href="territory.php" id="nav_territory">Territory</a></li>
        <li><a href="products.php" id="nav_products">Products</a></li>
        <!-- <li><a href="contact_us.php" class="email">Contact Us</a></li> -->
        <!-- <li><a href="#"><span title="info|wareindustrial.com" class="email">Contact Us</span></a></li> -->
      </ul>
    </nav>